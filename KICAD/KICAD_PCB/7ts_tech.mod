PCBNEW-LibModule-V1  02.04.2014 18:48:59
# encoding utf-8
Units mm
$INDEX
M2
M2,5
M3
M4
cutoff_hole
cutoff_hole_3
cutoff_hole_4
cutoff_hole_5
cutoff_hole_6
$EndINDEX
$MODULE M2
Po 0 0 0 15 533C3F74 00000000 ~~
Li M2
Sc 0
AR 
Op 0 0 0
T0 0 3 1 1 0 0.15 N V 21 N "M2"
T1 0 0 1 1 0 0.15 N V 21 N "~"
$PAD
Sh "0" C 3 3 0 0 0
Dr 2 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE M2
$MODULE M2,5
Po 0 0 0 15 533C3F65 00000000 ~~
Li M2,5
Sc 0
AR 
Op 0 0 0
T0 0 3 1 1 0 0.15 N V 21 N "M2,5"
T1 0 0 1 1 0 0.15 N V 21 N "~"
$PAD
Sh "0" C 3.5 3.5 0 0 0
Dr 2.5 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE M2,5
$MODULE M3
Po 0 0 0 15 533C3F3C 00000000 ~~
Li M3
Sc 0
AR 
Op 0 0 0
T0 0 3 1 1 0 0.15 N V 21 N "M3"
T1 0 0 1 1 0 0.15 N V 21 N "~"
$PAD
Sh "0" C 4 4 0 0 0
Dr 3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE M3
$MODULE M4
Po 0 0 0 15 533C3F06 00000000 ~~
Li M4
Sc 0
AR 
Op 0 0 0
T0 0 2.75 1 1 0 0.15 N V 21 N "M4"
T1 0 0 1 1 0 0.15 N V 21 N "~"
$PAD
Sh "0" C 5 5 0 0 0
Dr 4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE M4
$MODULE cutoff_hole
Po 0 0 0 15 5323AC18 00000000 ~~
Li cutoff_hole
Sc 0
AR 
Op 0 0 0
T0 0 0 1 1 0 0.15 N V 21 N "cutoff_hole"
T1 0 0 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "" R 10 1.27 0 0 0
Dr 10 0 0 O 10 1.27
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$EndMODULE cutoff_hole
$MODULE cutoff_hole_3
Po 0 0 0 15 5323AD70 00000000 ~~
Li cutoff_hole_3
Sc 0
AR 
Op 0 0 0
T0 0 -5 1 1 0 0.15 N V 21 N "cutoff_hole"
T1 0 5 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "" C 0.635 0.635 0 0 0
Dr 0.635 0 0 O 0.635 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" R 10 0.635 0 0 0
Dr 10 0 0 O 10 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po 6.75 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" R 10 0.635 0 0 0
Dr 10 0 0 O 10 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po -6.75 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" C 0.635 0.635 0 0 0
Dr 0.635 0 0 O 0.635 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po -1 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" C 0.635 0.635 0 0 0
Dr 0.635 0 0 O 0.635 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po 1 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$EndMODULE cutoff_hole_3
$MODULE cutoff_hole_4
Po 0 0 0 15 5323ADA9 00000000 ~~
Li cutoff_hole_4
Sc 0
AR 
Op 0 0 0
T0 0 -5 1 1 0 0.15 N V 21 N "cutoff_hole"
T1 0 5 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "" C 0.635 0.635 0 0 0
Dr 0.635 0 0 O 0.635 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" R 5 0.635 0 0 0
Dr 5 0 0 O 5 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po 3.75 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" R 5 0.635 0 0 0
Dr 5 0 0 O 5 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po -3.75 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" C 0.635 0.635 0 0 0
Dr 0.635 0 0 O 0.635 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po -0.75 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" C 0.635 0.635 0 0 0
Dr 0.635 0 0 O 0.635 0.635
At HOLE N 00E0FFFF
Ne 0 ""
Po 0.75 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$EndMODULE cutoff_hole_4
$MODULE cutoff_hole_5
Po 0 0 0 15 5323B2E4 00000000 ~~
Li cutoff_hole_5
Sc 0
AR 
Op 0 0 0
T0 0 -5 1 1 0 0.15 N V 21 N "cutoff_hole"
T1 0 5 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "" C 1.27 1.27 0 0 0
Dr 1.27 0 0 O 1.27 1.27
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" R 5.08 1.27 0 0 0
Dr 5 0 0 O 5 1.27
At HOLE N 00E0FFFF
Ne 0 ""
Po -5.08 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" C 1.27 1.27 0 0 0
Dr 1.27 0 0 O 1.27 1.27
At HOLE N 00E0FFFF
Ne 0 ""
Po -1.524 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" C 1.27 1.27 0 0 0
Dr 1.27 0 0 O 1.27 1.27
At HOLE N 00E0FFFF
Ne 0 ""
Po 1.524 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$EndMODULE cutoff_hole_5
$MODULE cutoff_hole_6
Po 0 0 0 15 5323B956 00000000 ~~
Li cutoff_hole_6
Sc 0
AR 
Op 0 0 0
T0 0 -5 1 1 0 0.15 N V 21 N "cutoff_hole"
T1 0 5 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "" C 1.25 1.25 0 0 0
Dr 1.25 0 0 O 1.25 1.25
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" O 10.16 1.25 0 0 0
Dr 10.16 0 0 O 10.16 1.25
At HOLE N 00E0FFFF
Ne 0 ""
Po -7.62 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" C 1.25 1.25 0 0 0
Dr 1.25 0 0 O 1.25 1.25
At HOLE N 00E0FFFF
Ne 0 ""
Po -1.524 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" C 1.25 1.25 0 0 0
Dr 1.25 0 0 O 1.25 1.25
At HOLE N 00E0FFFF
Ne 0 ""
Po 1.524 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$PAD
Sh "" O 10.16 1.25 0 0 0
Dr 10.16 0 0 O 10.16 1.25
At HOLE N 00E0FFFF
Ne 0 ""
Po 7.62 0
.SolderMask 0.001
.SolderPaste -0.001
.SolderPasteRatio -1e-008
.LocalClearance 0.001
.ZoneConnection 0
$EndPAD
$EndMODULE cutoff_hole_6
$EndLIBRARY
