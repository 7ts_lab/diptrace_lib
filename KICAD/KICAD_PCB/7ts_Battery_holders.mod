PCBNEW-LibModule-V1  14.04.2014 17:47:18
# encoding utf-8
Units mm
$INDEX
BAT_3000
bat_6.8
$EndINDEX
$MODULE BAT_3000
Po 0 0 0 15 534BF272 00000000 ~~
Li BAT_3000
Sc 0
AR 
Op 0 0 0
T0 0 3.25 1 1 0 0.15 N V 21 N "BAT_3000"
T1 0 -3 1 1 0 0.15 N V 21 N "VAL**"
DC 0 0 6 0 0.15 21
$PAD
Sh "2" R 4 4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "1" R 3.2 3.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7.87 0
$EndPAD
$PAD
Sh "1" R 3.2 3.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -7.87 0
$EndPAD
$SHAPE3D
Na "../../KICAD_3D/battery_holder_12mm_wthBatt.wrl"
Sc 0.4 0.4 0.4
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE BAT_3000
$MODULE bat_6.8
Po 0 0 0 15 534B1676 00000000 ~~
Li bat_6.8
Sc 0
AR 
Op 0 0 0
T0 0 1.4 1 1 0 0.15 N V 21 N "bat_6.8"
T1 0 -1.6 1 1 0 0.15 N V 21 N "VAL**"
DC 0 0 3.4 -0.1 0.15 21
$PAD
Sh "1" R 1.78 5.08 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.7 0
$EndPAD
$PAD
Sh "2" R 3.96 3.96 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "1" R 1.78 5.08 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.7 0
$EndPAD
$SHAPE3D
Na "../../KICAD_3D/battery_holder_6.8mm.wrl"
Sc 0.4 0.4 0.4
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE bat_6.8
$EndLIBRARY
