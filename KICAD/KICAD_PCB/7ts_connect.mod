PCBNEW-LibModule-V1  10.05.2014 14:13:45
# encoding utf-8
Units mm
$INDEX
2-pin,1.27
6-pin,1.27
BARREL_JACK
BARREL_JACK_1613_20
JTAG_10pin
PIN-18
PIN-20
RJ45_8_GND
RJ45_8_shielded
SIL-3-1,27
USB_MINI_B
USB_MINI_B_g
screw_terminal_2x5.08
screw_terminal_3x5.08
screw_terminal_4x5.08
$EndINDEX
$MODULE 2-pin,1.27
Po 0 0 0 15 52E409FA 00000000 ~~
Li 2-pin,1.27
Cd Connecteurs 2 pins
Kw CONN DEV
Sc 0
AR /52ACC35F
Op 0 0 0
T0 0 -2.032 1.72974 1.08712 0 0.3048 N V 21 N "P4"
T1 0 2.54 1.524 1.016 0 0.3048 N I 21 N "CONN_2"
DS -1.778 -1.016 -1.778 1.016 0.15 21
DS 1.778 -1.016 1.778 1.016 0.15 21
DS -1.778 -1.016 1.778 -1.016 0.15 21
DS 1.778 1.016 -1.778 1.016 0.15 21
$PAD
Sh "1" R 0.85 0.85 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 1 "5V"
Po -0.635 0
$EndPAD
$PAD
Sh "2" C 0.85 0.85 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 2 "GND"
Po 0.635 0
$EndPAD
$SHAPE3D
Na "connectors/conn_df13/df13-2p-125dsa.wrl"
Sc 2 2 2
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE 2-pin,1.27
$MODULE 6-pin,1.27
Po 0 0 0 15 532363D8 00000000 ~~
Li 6-pin,1.27
Cd Connecteur 6 pins
Kw CONN DEV
Sc 0
AR /531CF7AE
Op 0 0 0
T0 0 -2.54 1.72974 1.08712 0 0.3048 N V 21 N "P1"
T1 0 2.54 1.524 1.016 0 0.3048 N I 21 N "CONN_6"
DS -4.445 -1.27 4.445 -1.27 0.15 21
DS 4.445 -1.27 4.445 1.27 0.15 21
DS 4.445 1.27 -4.445 1.27 0.15 21
DS -4.445 1.27 -4.445 -1.27 0.15 21
$PAD
Sh "1" R 0.85 1.2 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 4 "N-000007"
Po -3.175 0
$EndPAD
$PAD
Sh "2" O 0.85 1.2 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 5 "N-000008"
Po -1.905 0
$EndPAD
$PAD
Sh "3" O 0.85 1.2 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 6 "N-000009"
Po -0.635 0
$EndPAD
$PAD
Sh "4" O 0.85 1.2 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 1 "N-0000010"
Po 0.635 0
$EndPAD
$PAD
Sh "5" O 0.85 1.2 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 2 "N-0000011"
Po 1.905 0
$EndPAD
$PAD
Sh "6" O 0.85 1.2 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 3 "N-000006"
Po 3.175 0
$EndPAD
$SHAPE3D
Na "connectors/conn_df13/df13-6p-125dsa.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 180
$EndSHAPE3D
$EndMODULE 6-pin,1.27
$MODULE BARREL_JACK
Po 0 0 0 15 52A7AFDF 00000000 ~~
Li BARREL_JACK
Cd DC Barrel Jack
Kw Power Jack
Sc 0
AR /52A7455C
Op 0 0 0
T0 10.09904 0 1.016 1.016 900 0.2032 N V 21 N "P2"
T1 0 -5.99948 1.016 1.016 0 0.2032 N I 21 N "CONN_2"
DS -4.0005 -4.50088 -4.0005 4.50088 0.381 21
DS -7.50062 -4.50088 -7.50062 4.50088 0.381 21
DS -7.50062 4.50088 7.00024 4.50088 0.381 21
DS 7.00024 4.50088 7.00024 -4.50088 0.381 21
DS 7.00024 -4.50088 -7.50062 -4.50088 0.381 21
$PAD
Sh "1" R 2 2.5 0 0 0
Dr 1.00076 0 0 O 1.00076 2.2
At STD N 00E0FFFF
Ne 2 "VIN"
Po 5 0
$EndPAD
$PAD
Sh "2" R 2 2.5 0 0 0
Dr 1.00076 0 0 O 1.00076 2.2
At STD N 00E0FFFF
Ne 1 "GND"
Po 0 0
$EndPAD
$PAD
Sh "3" R 2 2 0 0 0
Dr 1.5 0 0 O 1.5 0.80076
At STD N 00E0FFFF
Ne 1 "GND"
Po 2.5 2.8
$EndPAD
$EndMODULE BARREL_JACK
$MODULE BARREL_JACK_1613_20
Po 0 0 0 15 52A7B063 00000000 ~~
Li BARREL_JACK_1613_20
Cd DC Barrel Jack
Kw Power Jack
Sc 0
AR /52A7455C
Op 0 0 0
T0 10.09904 0 1.016 1.016 900 0.2032 N V 21 N "P2"
T1 0 -5.99948 1.016 1.016 0 0.2032 N I 21 N "CONN_2"
DS 4 3 5 3 0.15 21
DS 5 3 5 2 0.15 21
DS -6 0 -6 -3 0.15 21
DS -6 -3 5 -3 0.15 21
DS 5 -3 5 -2 0.15 21
DS -6 0 -6 3 0.15 21
DS -6 3 1 3 0.15 21
$PAD
Sh "1" R 2 2.5 0 0 0
Dr 1.00076 0 0 O 1.00076 2.2
At STD N 00E0FFFF
Ne 2 "VIN"
Po 5 0
$EndPAD
$PAD
Sh "2" R 2 2.5 0 0 0
Dr 1.00076 0 0 O 1.00076 2.2
At STD N 00E0FFFF
Ne 1 "GND"
Po 0 0
$EndPAD
$PAD
Sh "3" R 2 2 0 0 0
Dr 1.5 0 0 O 1.5 0.80076
At STD N 00E0FFFF
Ne 1 "GND"
Po 2.5 2.8
$EndPAD
$EndMODULE BARREL_JACK_1613_20
$MODULE JTAG_10pin
Po 0 0 0 15 52E4F45E 00000000 F~
Li JTAG_10pin
Cd Double rangee de contacts 2 x 5 pins
Kw CONN
Sc 0
AR /52CF32D3
Op 0 0 0
T0 0.635 -3.81 1.016 1.016 0 0.2032 N V 21 N "P2"
T1 0 -3.81 1.016 1.016 0 0.2032 N I 21 N "CONN_5X2"
DS 0 -4 -10 -4 0.15 21
DS -10 -4 -10 4 0.15 21
DS -10 4 0 4 0.15 21
DS 0 4 10 4 0.15 21
DS 10 4 10 -4 0.15 21
DS 10 -4 0 -4 0.15 21
$PAD
Sh "1" R 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 4 "TCK/SCK"
Po -5.08 1.27
$EndPAD
$PAD
Sh "2" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 2 "GND"
Po -5.08 -1.27
$EndPAD
$PAD
Sh "3" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 6 "TDO/MISO"
Po -2.54 1.27
$EndPAD
$PAD
Sh "4" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 1 "+5V"
Po -2.54 -1.27
$EndPAD
$PAD
Sh "5" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 7 "TMS"
Po 0 1.27
$EndPAD
$PAD
Sh "6" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -1.27
$EndPAD
$PAD
Sh "7" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 1 "+5V"
Po 2.54 1.27
$EndPAD
$PAD
Sh "8" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 3 "JTAG_nTRST"
Po 2.54 -1.27
$EndPAD
$PAD
Sh "9" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 5 "TDI/MOSI"
Po 5.08 1.27
$EndPAD
$PAD
Sh "10" C 1.524 1.524 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 2 "GND"
Po 5.08 -1.27
$EndPAD
$SHAPE3D
Na "headers/header_5x2.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE JTAG_10pin
$MODULE PIN-18
Po 0 0 0 15 536BEBF8 00000000 ~~
Li PIN-18
Cd Connecteur 18 pins
Kw CONN DEV
Sc 0
AR /536BC059
Op 0 0 0
T0 -12.7 -2.54 1.72974 1.08712 0 0.3048 N V 21 N "P1"
T1 8.89 -2.54 1.524 1.016 0 0.254 N V 21 N "PIN_18"
DS -22.86 -1.27 22.86 -1.27 0.3048 21
DS 22.86 -1.27 22.86 1.27 0.3048 21
DS 22.86 1.27 -22.86 1.27 0.3048 21
DS -22.86 1.27 -22.86 -1.27 0.3048 21
DS -20.32 -1.27 -20.32 1.27 0.3048 21
$PAD
Sh "1" R 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 17 "VCC"
Po -21.59 0
$EndPAD
$PAD
Sh "2" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 14 "RESET_P"
Po -19.05 0
$EndPAD
$PAD
Sh "3" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 13 "RESET_OUT_P"
Po -16.51 0
$EndPAD
$PAD
Sh "4" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 16 "USB_P"
Po -13.97 0
$EndPAD
$PAD
Sh "5" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 15 "USB_M"
Po -11.43 0
$EndPAD
$PAD
Sh "6" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 18 "WUA_P"
Po -8.89 0
$EndPAD
$PAD
Sh "7" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 4 "AC3"
Po -6.35 0
$EndPAD
$PAD
Sh "8" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 3 "AC2"
Po -3.81 0
$EndPAD
$PAD
Sh "9" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 2 "AC1"
Po -1.27 0
$EndPAD
$PAD
Sh "10" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 1 "AC0"
Po 1.27 0
$EndPAD
$PAD
Sh "11" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 12 "Ad7"
Po 3.81 0
$EndPAD
$PAD
Sh "12" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 11 "AD6"
Po 6.35 0
$EndPAD
$PAD
Sh "13" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 10 "AD5"
Po 8.89 0
$EndPAD
$PAD
Sh "14" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 9 "AD4"
Po 11.43 0
$EndPAD
$PAD
Sh "15" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 8 "AD3"
Po 13.97 0
$EndPAD
$PAD
Sh "16" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 7 "AD2"
Po 16.51 0
$EndPAD
$PAD
Sh "17" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 6 "AD1"
Po 19.05 0
$EndPAD
$PAD
Sh "18" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 5 "AD0"
Po 21.59 0
$EndPAD
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of 0.85 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of 0.75 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of 0.65 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of 0.55 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of 0.45 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of 0.35 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of 0.25 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of 0.15 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of 0.05 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of -0.05 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of -0.15 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of -0.25 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of -0.35 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of -0.45 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of -0.55 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of -0.65 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of -0.75 0 0
Ro 0 0 0
$EndSHAPE3D
$SHAPE3D
Na "grabCAD/AB2_HDR_M01-1V.wrl"
Sc 0.4 0.4 0.4
Of -0.85 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE PIN-18
$MODULE PIN-20
Po 0 0 0 15 536E17F6 00000000 ~~
Li PIN-20
Cd Connecteur 18 pins
Kw CONN DEV
Sc 0
AR /536E57BD
Op 0 0 0
T0 -12.7 -2.54 1.72974 1.08712 0 0.3048 N V 21 N "P2"
T1 8.89 -2.54 1.524 1.016 0 0.3048 N V 21 N "CONN_20"
$PAD
Sh "1" R 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 17 "GND"
Po -26.67 0
$EndPAD
$PAD
Sh "2" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 18 "PWEN"
Po -24.13 0
$EndPAD
$PAD
Sh "3" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 19 "SUSPEND#"
Po -21.59 0
$EndPAD
$PAD
Sh "4" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 9 "BD0"
Po -19.05 0
$EndPAD
$PAD
Sh "5" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 10 "BD1"
Po -16.51 0
$EndPAD
$PAD
Sh "6" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 11 "BD2"
Po -13.97 0
$EndPAD
$PAD
Sh "7" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 12 "BD3"
Po -11.43 0
$EndPAD
$PAD
Sh "8" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 13 "BD4"
Po -8.89 0
$EndPAD
$PAD
Sh "9" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 14 "BD5"
Po -6.35 0
$EndPAD
$PAD
Sh "10" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 15 "BD6"
Po -3.81 0
$EndPAD
$PAD
Sh "11" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 16 "BD7"
Po -1.27 0
$EndPAD
$PAD
Sh "12" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 20 "VIO"
Po 1.27 0
$EndPAD
$PAD
Sh "13" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 1 "BC0"
Po 3.81 0
$EndPAD
$PAD
Sh "14" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 2 "BC1"
Po 6.35 0
$EndPAD
$PAD
Sh "15" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 3 "BC2"
Po 8.89 0
$EndPAD
$PAD
Sh "16" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 4 "BC3"
Po 11.43 0
$EndPAD
$PAD
Sh "17" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 5 "BC4"
Po 13.97 0
$EndPAD
$PAD
Sh "18" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 6 "BC5"
Po 16.51 0
$EndPAD
$PAD
Sh "19" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 7 "BC6"
Po 19.05 0
$EndPAD
$PAD
Sh "20" C 1.397 1.397 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 8 "BC7"
Po 21.59 0
$EndPAD
$EndMODULE PIN-20
$MODULE RJ45_8_GND
Po 0 0 0 15 5335D40E 00000000 F~
Li RJ45_8_GND
Kw RJ45
Sc 0
AR /52E9333C
Op 0 0 0
T0 0.254 4.826 1.524 1.524 0 0.3048 N V 21 N "P2"
T1 0.14224 -0.1016 1.00076 1.00076 0 0.2032 N V 21 N "RJ45_ground"
DS -7.62 7.874 7.62 7.874 0.127 21
DS 7.62 7.874 7.62 -10.16 0.127 21
DS 7.62 -10.16 -7.62 -10.16 0.127 21
DS -7.62 -10.16 -7.62 7.874 0.127 21
$PAD
Sh "Hole" C 3.64998 3.64998 0 0 0
Dr 3.2512 0 0
At HOLE N 00F0FFFF
Ne 0 ""
Po 5.93852 0
$EndPAD
$PAD
Sh "Hole" C 3.64998 3.64998 0 0 0
Dr 3.2512 0 0
At HOLE N 00F0FFFF
Ne 0 ""
Po -5.9309 0
$EndPAD
$PAD
Sh "1" R 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 1 "N-0000019"
Po -4.445 -6.35
$EndPAD
$PAD
Sh "2" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 6 "N-0000030"
Po -3.175 -8.89
$EndPAD
$PAD
Sh "3" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 2 "N-0000021"
Po -1.905 -6.35
$EndPAD
$PAD
Sh "4" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 3 "N-0000027"
Po -0.635 -8.89
$EndPAD
$PAD
Sh "5" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 3 "N-0000027"
Po 0.635 -6.35
$EndPAD
$PAD
Sh "6" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 5 "N-0000029"
Po 1.905 -8.89
$EndPAD
$PAD
Sh "7" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 7 "N-0000031"
Po 3.175 -6.35
$EndPAD
$PAD
Sh "8" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 7 "N-0000031"
Po 4.445 -8.89
$EndPAD
$PAD
Sh "9" O 1.5 2.3 0 0 0
Dr 1 0 0 O 1 1.8
At STD N 00E0FFFF
Ne 4 "N-0000028"
Po -7.75 3
$EndPAD
$PAD
Sh "9" O 1.5 2.3 0 0 0
Dr 1 0 0 O 1 1.8
At STD N 00E0FFFF
Ne 4 "N-0000028"
Po 7.75 3
$EndPAD
$SHAPE3D
Na "connectors/RJ45_8.wrl"
Sc 0.4 0.4 0.4
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE RJ45_8_GND
$MODULE RJ45_8_shielded
Po 0 0 0 15 52EA6EC3 00000000 ~~
Li RJ45_8_shielded
Kw RJ45
Sc 0
AR /52E9333C
Op 0 0 0
T0 0.254 4.826 1.524 1.524 0 0.3048 N V 21 N "J1"
T1 0.14224 -0.1016 1.00076 1.00076 0 0.2032 N V 21 N "RJ45"
DS -7.62 7.874 7.62 7.874 0.127 21
DS 7.62 7.874 7.62 -10.16 0.127 21
DS 7.62 -10.16 -7.62 -10.16 0.127 21
DS -7.62 -10.16 -7.62 7.874 0.127 21
$PAD
Sh "Hole" C 3.64998 3.64998 0 0 0
Dr 3.2512 0 0
At HOLE N 00F0FFFF
Ne 0 ""
Po 5.93852 0
$EndPAD
$PAD
Sh "Hole" C 3.64998 3.64998 0 0 0
Dr 3.2512 0 0
At HOLE N 00F0FFFF
Ne 0 ""
Po -5.9309 0
$EndPAD
$PAD
Sh "1" R 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 4 "N-0000022"
Po -4.445 -6.35
$EndPAD
$PAD
Sh "2" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 3 "N-0000021"
Po -3.175 -8.89
$EndPAD
$PAD
Sh "3" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 1 "N-0000019"
Po -1.905 -6.35
$EndPAD
$PAD
Sh "4" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 6 "N-0000035"
Po -0.635 -8.89
$EndPAD
$PAD
Sh "5" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 6 "N-0000035"
Po 0.635 -6.35
$EndPAD
$PAD
Sh "6" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 2 "N-0000020"
Po 1.905 -8.89
$EndPAD
$PAD
Sh "7" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 5 "N-0000034"
Po 3.175 -6.35
$EndPAD
$PAD
Sh "8" C 1.50114 1.50114 0 0 0
Dr 0.89916 0 0
At STD N 00E0FFFF
Ne 5 "N-0000034"
Po 4.445 -8.89
$EndPAD
$PAD
Sh "9" R 1.3 2 0 0 0
Dr 1 0 0 O 1 1.8
At STD N 00E0FFFF
Ne 0 ""
Po -7.75 3
$EndPAD
$PAD
Sh "9" R 1.3 2 0 0 0
Dr 1 0 0 O 1 1.8
At STD N 00E0FFFF
Ne 0 ""
Po 7.75 3
$EndPAD
$SHAPE3D
Na "connectors/RJ45_8.wrl"
Sc 0.4 0.4 0.4
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE RJ45_8_shielded
$MODULE SIL-3-1,27
Po 0 0 0 15 52EFC3E3 00000000 ~~
Li SIL-3-1,27
Cd Connecteur 3 pins
Kw CONN DEV
Sc 0
AR /52EFC2CD
Op 0 0 0
T0 0 -3.81 1.7907 1.07696 0 0.3048 N V 21 N "K1"
T1 0 -3.81 1.524 1.016 0 0.3048 N I 21 N "CONN_3"
DS -2.54 1.27 2.54 1.27 0.15 21
DS -2.54 -1.905 -2.54 1.905 0.15 21
DS -2.54 1.905 2.54 1.905 0.15 21
DS 2.54 1.905 2.54 -1.905 0.15 21
DS 2.54 -1.905 -2.54 -1.905 0.15 21
$PAD
Sh "1" R 0.9 0.9 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 1 "GND"
Po -1.27 0
$EndPAD
$PAD
Sh "2" C 0.9 0.9 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 2 "MOSI"
Po 0 0
$EndPAD
$PAD
Sh "3" C 0.9 0.9 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 3 "VCC"
Po 1.27 0
$EndPAD
$EndMODULE SIL-3-1,27
$MODULE USB_MINI_B
Po 0 0 0 15 52C4A8AB 00000000 ~~
Li USB_MINI_B
Cd USB Mini-B 5-pin SMD connector
Kw USB, Mini-B, connector
Sc 0
AR /52AED56D
Op 0 0 0
T0 0 6.90118 1.016 1.016 0 0.2032 N V 21 N "J1"
T1 0 -7.0993 1.016 1.016 0 0.2032 N I 21 N "USB"
DS -3.59918 -3.85064 -3.59918 3.85064 0.381 21
DS -4.59994 -3.85064 -4.59994 3.85064 0.381 21
DS -4.59994 3.85064 4.59994 3.85064 0.381 21
DS 4.59994 3.85064 4.59994 -3.85064 0.381 21
DS 4.59994 -3.85064 -4.59994 -3.85064 0.381 21
$PAD
Sh "1" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 4 "N-0000060"
Po 3.44932 -1.6002
$EndPAD
$PAD
Sh "2" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "N-0000051"
Po 3.44932 -0.8001
$EndPAD
$PAD
Sh "3" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "N-0000057"
Po 3.44932 0
$EndPAD
$PAD
Sh "4" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 3.44932 0.8001
$EndPAD
$PAD
Sh "5" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 3.44932 1.6002
$EndPAD
$PAD
Sh "6" R 2.49936 1.99898 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 3.35026 -4.45008
$EndPAD
$PAD
Sh "7" R 2.49936 1.99898 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po -2.14884 -4.45008
$EndPAD
$PAD
Sh "8" R 2.49936 1.99898 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 3.35026 4.45008
$EndPAD
$PAD
Sh "9" R 2.49936 1.99898 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po -2.14884 4.45008
$EndPAD
$PAD
Sh "" C 0.89916 0.89916 0 0 0
Dr 0.89916 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0.8509 -2.19964
$EndPAD
$PAD
Sh "" C 0.89916 0.89916 0 0 0
Dr 0.89916 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0.8509 2.19964
$EndPAD
$EndMODULE USB_MINI_B
$MODULE USB_MINI_B_g
Po 0 0 0 15 52C58AB4 00000000 ~~
Li USB_MINI_B_g
Cd USB Mini-B 5-pin SMD connector
Kw USB, Mini-B, connector
Sc 0
AR /52AED56D
Op 0 0 0
T0 0 6.90118 1.016 1.016 0 0.2032 N V 21 N "J1"
T1 0 -7.0993 1.016 1.016 0 0.2032 N I 21 N "USB"
DS -3.59918 -3.85064 -3.59918 3.85064 0.381 21
DS -4.59994 -3.85064 -4.59994 3.85064 0.381 21
DS -4.59994 3.85064 4.59994 3.85064 0.381 21
DS 4.59994 3.85064 4.59994 -3.85064 0.381 21
DS 4.59994 -3.85064 -4.59994 -3.85064 0.381 21
$PAD
Sh "1" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 4 "N-0000060"
Po 3.44932 -1.6002
$EndPAD
$PAD
Sh "2" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "N-0000051"
Po 3.44932 -0.8001
$EndPAD
$PAD
Sh "3" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "N-0000057"
Po 3.44932 0
$EndPAD
$PAD
Sh "4" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 3.44932 0.8001
$EndPAD
$PAD
Sh "5" R 2.30124 0.50038 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 3.44932 1.6002
$EndPAD
$PAD
Sh "6" R 2.49936 1.99898 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 3.35026 -4.45008
$EndPAD
$PAD
Sh "7" R 2.49936 1.99898 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po -2.14884 -4.45008
$EndPAD
$PAD
Sh "8" R 2.49936 1.99898 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 3.35026 4.45008
$EndPAD
$PAD
Sh "9" R 2.49936 1.99898 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po -2.14884 4.45008
$EndPAD
$PAD
Sh "" C 0.89916 0.89916 0 0 0
Dr 0.89916 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0.8509 -2.19964
$EndPAD
$PAD
Sh "" C 0.89916 0.89916 0 0 0
Dr 0.89916 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0.8509 2.19964
$EndPAD
$EndMODULE USB_MINI_B_g
$MODULE screw_terminal_2x5.08
Po 0 0 0 15 52C74C6D 00000000 ~~
Li screw_terminal_2x5.08
Sc 0
AR 
Op 0 0 0
T0 0 3 1 1 0 0.15 N V 21 N "screw_terminal_2x5.08"
T1 0 -3 1 1 0 0.15 N V 21 N "VAL**"
DS -5.08 -5.08 5.08 -5.08 0.15 21
DS 5.08 -5.08 5.08 5.08 0.15 21
DS -5.08 -5.08 -5.08 5.08 0.15 21
DS -5 5 5 5 0.15 21
$PAD
Sh "1" C 2.8 2.8 0 0 0
Dr 1.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.54 0
$EndPAD
$PAD
Sh "2" C 2.8 2.8 0 0 0
Dr 1.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2.54 0
$EndPAD
$EndMODULE screw_terminal_2x5.08
$MODULE screw_terminal_3x5.08
Po 0 0 0 15 52C74CF5 00000000 ~~
Li screw_terminal_3x5.08
Sc 0
AR 
Op 0 0 0
T0 0 3 1 1 0 0.15 N V 21 N "screw_terminal_3x5.08"
T1 0 -3 1 1 0 0.15 N V 21 N "VAL**"
DS -7.62 5.08 2.54 5.08 0.15 21
DS 2.54 5.08 7.62 5.08 0.15 21
DS 2.54 -5.08 7.62 -5.08 0.15 21
DS -7.62 -5.08 2.54 -5.08 0.15 21
DS 7.62 -5.08 7.62 5.08 0.15 21
DS -7.62 -5.08 -7.62 5.08 0.15 21
$PAD
Sh "1" C 2.8 2.8 0 0 0
Dr 1.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.08 0
$EndPAD
$PAD
Sh "2" C 2.8 2.8 0 0 0
Dr 1.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "3" C 2.8 2.8 0 0 0
Dr 1.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.08 0
$EndPAD
$EndMODULE screw_terminal_3x5.08
$MODULE screw_terminal_4x5.08
Po 0 0 0 15 52C74D16 00000000 ~~
Li screw_terminal_4x5.08
Sc 0
AR 
Op 0 0 0
T0 0 3 1 1 0 0.15 N V 21 N "screw_terminal_4x5.08"
T1 0 -3 1 1 0 0.15 N V 21 N "VAL**"
DS -5.08 5.08 5.08 5.08 0.15 21
DS 5.08 5.08 10.16 5.08 0.15 21
DS -10.16 5.08 -5.08 5.08 0.15 21
DS 5.08 -5.08 10.16 -5.08 0.15 21
DS -10.16 -5.08 -5.08 -5.08 0.15 21
DS -5.08 -5.08 5.08 -5.08 0.15 21
DS 10.16 -5.08 10.16 5.08 0.15 21
DS -10.16 -5.08 -10.16 5.08 0.15 21
$PAD
Sh "1" C 2.8 2.8 0 0 0
Dr 1.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7.62 0
$EndPAD
$PAD
Sh "2" C 2.8 2.8 0 0 0
Dr 1.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.54 0
$EndPAD
$PAD
Sh "3" C 2.8 2.8 0 0 0
Dr 1.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2.54 0
$EndPAD
$PAD
Sh "4" C 2.8 2.8 0 0 0
Dr 1.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -7.62 0
$EndPAD
$EndMODULE screw_terminal_4x5.08
$EndLIBRARY
