PCBNEW-LibModule-V1  27.03.2014 0:44:14
# encoding utf-8
Units mm
$INDEX
8-bit_switch
MQ_A
encoder_2ch
pot_P110KH
$EndINDEX
$MODULE 8-bit_switch
Po 0 0 0 15 52EED361 00000000 ~~
Li 8-bit_switch
Sc 0
AR 
Op 0 0 0
T0 0 1.06 1 1 0 0.15 N V 21 N "8-bit_switch"
T1 0 -1.016 1 1 0 0.15 N V 21 N "VAL**"
DS 9.906 -2.54 9.906 2.54 0.15 21
DS 9.906 2.54 -9.906 2.54 0.15 21
DS -9.906 2.54 -9.906 -2.54 0.15 21
DS -9.906 -2.54 0 -2.54 0.15 21
DS 0 -2.54 9.906 -2.54 0.15 21
$PAD
Sh "1" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -8.89 4.125
$EndPAD
$PAD
Sh "2" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.35 4.125
$EndPAD
$PAD
Sh "3" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.81 4.125
$EndPAD
$PAD
Sh "4" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.27 4.125
$EndPAD
$PAD
Sh "5" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.27 4.125
$EndPAD
$PAD
Sh "6" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.81 4.125
$EndPAD
$PAD
Sh "7" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.35 4.125
$EndPAD
$PAD
Sh "8" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 8.89 4.125
$EndPAD
$PAD
Sh "9" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 8.89 -4.125
$EndPAD
$PAD
Sh "10" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.35 -4.125
$EndPAD
$PAD
Sh "11" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.81 -4.125
$EndPAD
$PAD
Sh "12" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.27 -4.125
$EndPAD
$PAD
Sh "13" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.27 -4.125
$EndPAD
$PAD
Sh "14" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.81 -4.125
$EndPAD
$PAD
Sh "15" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.35 -4.125
$EndPAD
$PAD
Sh "16" R 1.1 2.15 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -8.89 -4.125
$EndPAD
$EndMODULE 8-bit_switch
$MODULE MQ_A
Po 0 0 0 15 53335825 00000000 ~~
Li MQ_A
Sc 0
AR /53332718
Op 0 0 0
T0 0 -7.5 1 1 0 0.15 N V 21 N "S1"
T1 0 7.25 1 1 0 0.15 N V 21 N "MQ_GAS_SENSOR"
DC 0 0 9.25 0 0.15 21
DC 0 0 9.75 0 0.15 21
$PAD
Sh "2" C 2 2 0 0 0
Dr 1.2 0 0
At STD N 00E0FFFF
Ne 2 "N-000003"
Po 3.36 -3.36
$EndPAD
$PAD
Sh "2" C 2 2 0 0 0
Dr 1.2 0 0
At STD N 00E0FFFF
Ne 2 "N-000003"
Po -3.36 -3.36
$EndPAD
$PAD
Sh "1" C 2 2 0 0 0
Dr 1.2 0 0
At STD N 00E0FFFF
Ne 3 "VCC"
Po -3.36 3.36
$EndPAD
$PAD
Sh "1" C 2 2 0 0 0
Dr 1.2 0 0
At STD N 00E0FFFF
Ne 3 "VCC"
Po 3.36 3.36
$EndPAD
$PAD
Sh "3" C 2 2 0 0 0
Dr 1.2 0 0
At STD N 00E0FFFF
Ne 1 "GND"
Po 0 -4.75
$EndPAD
$PAD
Sh "4" C 2 2 0 0 0
Dr 1.2 0 0
At STD N 00E0FFFF
Ne 3 "VCC"
Po 0 4.75
$EndPAD
$SHAPE3D
Na "../../KICAD_3D/MQ_GasSensor.wrl"
Sc 0.4 0.4 0.4
Of 0 0 0
Ro 180 0 0
$EndSHAPE3D
$EndMODULE MQ_A
$MODULE encoder_2ch
Po 0 0 0 15 52A459EF 00000000 ~~
Li encoder_2ch
Sc 0
AR /52A211AF
Op 0 0 0
T0 0 7.5 1 1 0 0.15 N V 21 N "E1"
T1 0 -10 1 1 0 0.15 N V 21 N "ENC"
$PAD
Sh "1" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 2 "N-0000040"
Po -2.5 5
$EndPAD
$PAD
Sh "2" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 1 "GND"
Po 0 5
$EndPAD
$PAD
Sh "3" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 3 "N-0000043"
Po 2.5 5
$EndPAD
$PAD
Sh "" R 2.4 2.4 0 0 0
Dr 2 0 0 O 2 2.1
At HOLE N 00E0FFFF
Ne 0 ""
Po 6.2 -2.5
$EndPAD
$PAD
Sh "" R 2.4 2.4 0 0 0
Dr 2 0 0 O 2 2.1
At HOLE N 00E0FFFF
Ne 0 ""
Po -6.2 -2.5
$EndPAD
$EndMODULE encoder_2ch
$MODULE pot_P110KH
Po 0 0 0 15 52A3AE82 00000000 ~~
Li pot_P110KH
Sc 0
AR 
Op 0 0 0
T0 0 9 1 1 0 0.15 N V 21 N "pot_P110KH"
T1 0 -5 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "2" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 7
$EndPAD
$PAD
Sh "3" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.5 7
$EndPAD
$PAD
Sh "1" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2.5 7
$EndPAD
$PAD
Sh "" R 1.8 2.2 0 0 0
Dr 1.8 0 0 O 1.8 2.2
At HOLE N 00E0FFFF
Ne 0 ""
Po -4.4 0
$EndPAD
$PAD
Sh "" R 1.8 2.2 0 0 0
Dr 1.8 0 0 O 1.8 2.2
At HOLE N 00E0FFFF
Ne 0 ""
Po 4.4 0
$EndPAD
$EndMODULE pot_P110KH
$EndLIBRARY
